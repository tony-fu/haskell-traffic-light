var ohm = require('ohm-js')

var myGrammar = ohm.grammar(`Signature {
    Exp = Body
    arrow = "->"
    typeVar = lower digit*
    concrete = upper lower*
    Type = typeVar | concrete
    SimpleStructure = Type Type
    MultiStructure = Type "(" SimpleStructure ")"
    Structure = SimpleStructure | MultiStructure | Type
    Function = Structure (arrow Structure)+
    ParrenStructure = "(" Function ")" 
    BodyPart = Structure | ParrenStructure
    Body = BodyPart (arrow BodyPart)*
}
`);

let semantics = myGrammar.createSemantics().addOperation('eval', {
    Exp: (e: { eval: () => any; }) => e.eval(),
    Body: (a: { eval: () => any; }, b: any, c: { numChildren: number; eval: () => any; }) => {
        if (c.numChildren > 0) {
            return { "func": [a.eval(), ...c.eval()] }
        } else {
            return a.eval()
        }
    },
    BodyPart: (e: { eval: () => any; }) => e.eval(),
    Function: (a: { eval: () => any; }, b: any, c: { eval: () => any; }) => ({ "func": [a.eval(), ...c.eval()] }),
    ParrenStructure: (x: any, e: { eval: () => any; }, y: any) => e.eval(),
    SimpleStructure: (a: any, b: any) => [a.eval(), b.eval()],
    MultiStructure: (a: any, b: any, c: any, d: any) => [a.eval(), ...c.eval()],
    Structure: (a: any) => {
        if (a['ctorName'] === 'MultiStructure') {
            return { 'struc': a.eval() }
        } else if (a['ctorName'] === 'SimpleStructure') {
            return { 'struc': a.eval() }
        } else {
            return { 'simp': a.eval() }
        }
    },
    Type: (e: { eval: () => any; }) => e.eval(),
    concrete: (a: { sourceString: any; }, b: { sourceString: any; }) => a.sourceString + b.sourceString,
    typeVar: (a: any, b: any) => a.sourceString + b.sourceString
})

function parse(sig: string): any {
    let match = myGrammar.match(sig);
    return match.succeeded ? semantics(match).eval() : {}
}

export function parseSignature(input: string) {
    if (input.includes("=>")) {
        let [_, body] = input.split(" => ")
        return parse(body)
    } else {
        let [_, body] = input.split(" :: ")
        return parse(body)
    }
}