import * as vscode from 'vscode'
const { exec } = require('child_process')
import { parseSignature } from "./parseSignature.js"

const getTypeSig = function (words: string, filename: string, uri: vscode.Uri) {
	exec(`stack ghc -- -e ":t (${words})" ${filename}`, function (error: any, stdout: string, stderr: any) {
		if (error) {
			console.log(error)
			vscode.window.showErrorMessage("GHC errors")
		}
		let result = parseSignature(stdout)
		const panel = vscode.window.createWebviewPanel(
			"Types",
			"Selected Types",
			vscode.ViewColumn.Two,
			{}
		)
		panel.webview.html = getWebviewContent(result, uri, panel.webview);
	})
}

export function activate(context: vscode.ExtensionContext) {
	context.subscriptions.push(
		vscode.commands.registerCommand('types.render', () => {
			const editor = vscode.window.activeTextEditor
			if (editor) {
				const document = editor.document
				const filename = document.fileName
				const selection = editor.selection
				const words = document.getText(selection)
				getTypeSig(words, filename, context.extensionUri)
			}
		})
	);
}

function renderTree(tree: any): string {
	if (tree.hasOwnProperty('simp')) {
		return renderSimp(tree)
	} else if (tree.hasOwnProperty('struc')) {
		return renderStruc(tree)
	} else if (tree.hasOwnProperty('func')) {
		return renderFunc(tree)
	}
	return ""
}

function renderSimp(tree: any): string {
	let symbol = tree['simp']
	return `
	<div class="simp ${symbol}"></div>
	`
}

function renderStruc(tree: any): string {
	let symbols = tree['struc']
	let heads = symbols.filter((_: any, i: number) => i < symbols.length - 1)
	let last = symbols[symbols.length - 1]
	let opens = heads.map((head: string) => `<div class="struc ${head}">`).join("")
	let closes = heads.map((head: string) => "</div>").join("")
	return opens + `<div class="simp ${last}"></div>` + closes
}

function renderFunc(tree: any): string {
	let functree = tree['func']
	let args = functree.filter((_: any, i: number) => i < functree.length - 1)
	let result = functree[functree.length - 1]
	return `
	<div class="func">
		<div class="func-body">
			${args.map((arg: any) => renderTree(arg)).join("")}
		</div>
		<div class="connector"></div>
		<div class="func-return">
			${renderTree(result)}
		</div>
	</div>
	`
}

function getWebviewContent(tree: any, uri: vscode.Uri, webview: vscode.Webview) {
	const stylesPathMainPath = vscode.Uri.joinPath(uri, 'media', 'vscode.css');
	const stylesMainUri = webview.asWebviewUri(stylesPathMainPath);
	return `<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Type Graph</title>
		<link href="${stylesMainUri}" rel="stylesheet">
	</head>
	<body>
	<div class="type-graph">
		${renderTree(tree)}
	</div>
	</body>
	</html>`
}


