# Traffic light diagram: an diagrammatic view for haskell types


## Demo

![demo1](demo1.gif)

![demo2](demo2.gif)

![demo3](demo3.gif)


## Running the example

- Open this example in VS Code 1.47+
- `npm install`
- `npm run watch` or `npm run compile`
- `F5` to start debugging

Run the `Haskell: Render Haskell Type as Graph` to create the webview.
